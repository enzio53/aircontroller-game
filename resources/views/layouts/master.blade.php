<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>AirController | @yield('title')</title>

        <meta name="viewport" content="width=device-width, user-scalable=no" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/css/leaflet.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}" />

        <script type="text/javascript" src="{{ asset('assets/js/leaflet.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/leaflet.ajax.min.js') }}"></script>
    </head>
    <body class="text-white">
        <nav class="navbar navbar-light bg-gradient-dark shadow-lg">
            <div class="navbar-brand"></div>

            <img class="navbar-brand align-content-center" src="{{ asset('assets/img/royal_air_force_96px.png') }}" width="50" height="55" alt="" />

            <div class="form-inline">
                <a class="navbar-brand">
                    v 0.1
                </a>
            </div>
        </nav>

        <div class="text-black-50" id="map"></div>

        <div class="container-fluid text-black-50 mt-3">
            @yield('content')
        </div>

        <script type="text/javascript" src="{{ asset('assets/js/jquery-3.4.1.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script type="text/javascript">
            let BasesIcons = L.icon({
                iconUrl: '{{ asset('assets/img/royal_air_force_96px.png') }}',
                iconSize: [38, 38]
            });
        </script>

        <script type="text/javascript" src="{{ asset('build/js/main.js') }}"></script>
    </body>
</html>
