@extends('layouts.master')

@section('title')
    Start Game
@endsection

@section('content')
    <div class="row text-white">
        <div class="col-lg-2 mb-5">
            <div class="card">
                <div class="card-body text-center">
                    <img src="{{ asset('assets/img/06_plane_00002.png') }}" alt="" />
                    <span class="display-1">100</span>
                    <h4 class="lead">Décollages sur alerte</h4>
                </div>
            </div>
        </div>

        <div class="col-lg-6 mb-5">
            <div class="card">
                <div class="card-header">
                    <img class="align-middle" src="{{ asset('assets/img/air_traffic_control_tower_filled_100px.png') }}" height="30" width="30" alt="" />
                    <span class="lead align-middle">ATC</span>

                    <div class="float-right">
                        <ul class="nav nav-tabs card-header-tabs">
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="collapse-xhr" href="#gestionTraffic">Gestion du Traffic</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link">Gestion des départs</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="collapse" id="gestionTraffic">
                    <div class="card-body">
                        <h4 class="lead text-center">Gestion du Traffic</h4>

                        <div class="text-center align-middle">
                            <strong class="lead">nothing to show</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        Echo.channel('home')
            .listen('NewMessage', (event) => {
                console.log(event.message);
            });
    </script>
@endsection
