function getRegionColors(region) {
    if (registrar[region]) {
        if (bases[registrar[region]]) {
            let base = bases[registrar[region]];

            return base.planes.mirage > 4 ? '#27ae60' :
                base.planes.mirage > 2 ? '#8e44ad' :
                    base.planes.mirage === 0 || base.planes.mirage === 1 ? '#c0392b' : '#c0392b';
        }
    }

    return '#c0392b';
}

function setStyleMap(feature) {
    let regionStyle = module.exports.getRegionColors(feature.properties.nom);

    return {
        fillColor: regionStyle,
        weight: 2,
        opacity: 1,
        color: '#bdc3c7',
        dashArray: '3',
        fillOpacity: 0.7
    };
}

module.exports = {
    getRegionColors: getRegionColors,
    setStyleMap: setStyleMap
};
