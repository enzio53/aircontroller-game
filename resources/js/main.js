import Echo from 'laravel-echo';
import gameManager from './class/gameManager.class';
import helpers from './funcs/helpers';

window.registrar = {
    'Nouvelle-Aquitaine': 'base_106'
};

window.bases = {
    base_106: {
        details: {
            AITA: 'BOD',
            OACI: 'LFBD',
            coordinate: {
                latitude: 44.8374,
                longitude: -0.6853
            }
        },
        planes: {
            mirage: 5
        }
    }
};

window.currentFlying = {};

window.auth = {
    user: 'enzio',
    gid: 'FRAa98db973kwl8xp1lz94k'
};

window.Pusher = require('pusher-js');
window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    cluster: process.env.MIX_PUSHER_APP_CLUSTER,
    // encrypted: true,
    wsHost: window.location.hostname,
    wsPort: 6001
});

let InstanceGameManager = new gameManager();

let map = L.map('map')
    .setView([
        46.525,
        2.71
    ], 5);

let layerGeo = new L.GeoJSON.AJAX('https://raw.githubusercontent.com/gregoiredavid/france-geojson/master/regions-avec-outre-mer.geojson', {style: helpers.setStyleMap});
layerGeo.addTo(map);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZW56aW81MyIsImEiOiJjazBjengyeHAwMG1mM2R0aTB2ejN6cmVvIn0.Hm0K2SFFKiZCFkw2MGVTvQ\n', {
    attribution: '',
    maxZoom: 18,
    id: 'mapbox.satellite',
    accessToken: 'pk.eyJ1IjoiZW56aW81MyIsImEiOiJjazBjengyeHAwMG1mM2R0aTB2ejN6cmVvIn0.Hm0K2SFFKiZCFkw2MGVTvQ'
}).addTo(map);

jQuery.each(bases, function () {
    L.marker([
        this.details.coordinate.latitude,
        this.details.coordinate.longitude
    ], {icon: BasesIcons}).addTo(map);
});

$(document).ready(() => {
    $("[data-toggle='collapse-xhr']").click((event) => {
        event.preventDefault();

        console.log(this.data());
    });
});
