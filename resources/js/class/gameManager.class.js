class gameManage {

    constructor() {}

    get getAllPlanesFlying() {
        return currentFlying;
    }

    set getFlyingPlaneFromSpecificBase(base) {
        if (currentFlying[base]) {
            return currentFlying[base];
        }

        return false;
    }

}

module.exports = gameManage;
