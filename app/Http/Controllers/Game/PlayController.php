<?php

namespace App\Http\Controllers\Game;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class PlayController extends Controller
{

    public function index(Request $request, Response $response)
    {
        return view('game.index');
    }

}
